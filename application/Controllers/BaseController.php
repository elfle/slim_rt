<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 11.11.14.
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */

namespace App\Controllers;

use Slim\Slim;

class BaseController
{
	public function __construct()
	{
		$this->slimApp = Slim::getInstance();
	}
}
