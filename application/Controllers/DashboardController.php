<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */

namespace App\Controllers;

use App\Core\LoggedInUser;
use App\Models\Menu;
use Slim\Slim;

class DashboardController extends BaseController
{
	public function index()
	{
		$this->slimApp->render(
			'dashboard/index.php',
			array(
				'menu_items' => Menu::getMenuForLoggedInUser(),
				'logged_user' => LoggedInUser::getUserData()
			)
		);
	}
}
