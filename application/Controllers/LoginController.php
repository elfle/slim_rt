<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */

namespace App\Controllers;

use App\Core\LoggedInUser;
use Slim\Slim;

class LoginController extends BaseController
{
	public function ajaxLogin()
	{
		$response = ['success' => 1, 'login_valid' => $this->attemptLoginFromRequest()];

		$this->slimApp->response->write(json_encode($response));
	}

	public function login()
	{
		$this->slimApp->render(
			'login/form.php',
			array( 'name' => 'Josh' )
		);
	}

	/**
	 * @return bool
	 */
	private function attemptLoginFromRequest()
	{
		$request = $this->slimApp->request;

		// check input, try to login
		$username = $request->params('username');
		$password = $request->params('password');

		return LoggedInUser::attemptLogin($username, $password);
	}
}
