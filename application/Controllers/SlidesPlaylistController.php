<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */

namespace App\Controllers;

use App\Core\LoggedInUser;
use App\Helpers\Response;
use App\Models\Playlist;
use App\Models\PlaylistItem;
use App\Models\Resource;
use Slim\Slim;

class SlidesPlaylistController extends BaseController
{
	public function index()
	{
		$this->showList();
	}

	public function showList()
	{
		$model = new Playlist();

		Response::ajaxTemplate(
			'slides_playlist/list.php',
			array(
				'list' => $model->getListForUser(LoggedInUser::getUserId())
			)
		);
	}

	// loads data for resources
	public function load()
	{
		$playlist_id = $this->slimApp->request->params('id', 0);

		$resourceModel = new Resource();
		$resources = $resourceModel->loadForUserId(LoggedInUser::getUserId());

		// load playlist
		$playlistModel = new Playlist();
		$playlistData = $playlistModel->load($playlist_id);

		// load items
		$playlistItems = empty($playlistData) ? array() : (new PlaylistItem())->loadByPlaylistId($playlist_id);

		// inject absolute URL
		// @todo. change this for production?
		$url_base = '';
		foreach($resources as &$r)
		{
			$r['resource_url'] = $url_base . $r['location'];
		}
		foreach($playlistItems as &$i)
		{
			$i['resource_url'] = $url_base . $i['location'];
		}

		Response::ajax(array(
			'resources' => $resources,
			'playlist' => $playlistData,
			'items' => $playlistItems
		));
	}

	public function save()
	{
		// @todo: validate
		$request = $this->slimApp->request;
		$playlist_data = $request->params('playlist', array());
		$id = isset($playlist_data['id']) ? $playlist_data['id'] : 0;
		$title = isset($playlist_data['title']) ? $playlist_data['title'] : 'bez imena';
		$owner_id = LoggedInUser::getUserId();
		$items = $request->params('items', array());

		$model = new Playlist();

		if($id > 0)
		{
			$record_id = $model->update($id, $title, $owner_id, $items);
		}
		else
		{
			$record_id = $model->create($title, $owner_id, $items);
		}

		$response = array(
			'success' => 0
		);

		if($record_id > 0)
		{
			$response = array(
				'success' => 1,
				'id' => $record_id ?: 0
			);
		}

		Response::ajax($response);
	}
}
