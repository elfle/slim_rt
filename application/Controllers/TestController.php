<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 08.11.14.
 * Time: 19:56
 * To change this template use File | Settings | File Templates.
 */

namespace App\Controllers;

use Slim\Slim;

class TestController {
	public function index()
	{
		Slim::getInstance()->render(
			'testTemplate.php',
			array( 'name' => 'Josh' )
		);
	}
}
