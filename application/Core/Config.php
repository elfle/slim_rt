<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */

namespace App\Core;


class Config {
	public static function get($key = 'db')
	{
		// just return default config, implement environments later if needed

		$result = array();
		$file_path = PATH_APP . '/config/' . $key . '.php';

		if(file_exists($file_path))
		{
			$result = require $file_path;
		}

		return $result;
	}
}
