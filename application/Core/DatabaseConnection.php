<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */

namespace App\Core;


use Slim\Slim;

class DatabaseConnection
{
	private static $connection;

	/**
	 * return a PDO object connected to the DB
	 *
	 * @throws \PDOException
	 * @throws \Exception
	 *
	 * @return \PDO
	 */
	public static function getConnection()
	{
		try
		{
			if(!self::$connection instanceof \PDO)
			{
				$config = Config::get();

				$host = isset($config['host']) ? $config['host'] : 'localhost';
				$dbname = isset($config['dbname']) ? $config['dbname'] : '';
				$user = isset($config['user']) ? $config['user'] : '';
				$pass = isset($config['pass']) ? $config['pass'] : '';

				if(!$host || !$dbname || !$user)
				{
					throw new \Exception('Unable to connect to the database, missing configuration data');
				}

				self::$connection = new \PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);
			}

			return self::$connection;
		}
		catch(\PDOException $ex)
		{
			// throw a new exception so DB config is not revealed
			throw new \PDOException('Unable to connect to the database');
		}
	}
}
