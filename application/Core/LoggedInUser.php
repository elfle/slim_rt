<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */

namespace App\Core;


use App\Models\User;

class LoggedInUser
{
	private static $instance = null;

	private function __construct()
	{
		// @todo: read from session

		session_start();
	}
	public function __clone(){}
	public function __wakeup(){}

	/**
	 * @return LoggedInUser|null
	 */
	public static function getInstance()
	{
		if(!self::$instance instanceof self)
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

	public static function getUserData()
	{
		return self::getInstance()->getData();
	}

	public static function getUserId()
	{
		$data = self::getUserData();
//		return !empty($data) && isset($data['id']) ? $data['id'] : 0;

		// hmmm, primary key is not id but userId....
		return !empty($data) && isset($data['userId']) ? $data['userId'] : 0;
	}

	/**
	 * is the user logged-in
	 *
	 * @return bool
	 */
	public static function isLoggedIn()
	{
		$userData = self::getInstance()->getData();
		return !empty($userData);
	}

	/**
	 * @param string $username
	 * @param string $password
	 *
	 * @return bool
	 */
	public static function attemptLogin($username, $password)
	{
		$user = new User();

		$userData = $user->getUserByUsernameAndPassword($username, $password);

		if($userData)
		{
			self::getInstance()->setData($userData);

			return true;
		}

		return false;
	}

	public static function logOut()
	{
		self::getInstance()->clearData();
	}

	protected function setData(array $data)
	{
		$_SESSION['logged_in_user'] = $data;
	}

	protected function getData()
	{
		return isset($_SESSION['logged_in_user']) ? $_SESSION['logged_in_user'] : [];
	}

	protected function clearData()
	{
		$_SESSION['logged_in_user'] = [];
	}

	/**
	 * hash the password string
	 *
	 * @param $plainPassword
	 * @return mixed
	 */
	protected static function hashPassword($plainPassword)
	{
		return $plainPassword;
	}
}
