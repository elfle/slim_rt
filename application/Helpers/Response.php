<?php

namespace App\Helpers;

/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 06.12.14.
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */

class Response
{
	public static function ajaxTemplate($template, $templateData = array(), $status = null)
	{
		$app = \Slim\Slim::getInstance();
		$view = $app->view;

		if (!is_null($status)) {
			$app->response->status($status);
		}

		$content = $view->fetch($template, $templateData);

		self::ajax(array(
			'success' => true,
			'content' => $content
		));
	}

	public static function ajax($data = array())
	{
		echo json_encode($data);
	}
}
