<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


use App\Core\DatabaseConnection;

class BaseModel
{
	protected $table = '';

	/**
	 * @var \PDO
	 */
	protected $pdo;

	public function __construct()
	{
		$this->pdo = DatabaseConnection::getConnection();
	}

	public function getTable()
	{
		return $this->table;
	}
}
