<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 11.11.14.
 * Time: 20:49
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


use App\Core\LoggedInUser;

class Menu extends BaseModel
{
	public function getMenuForUserId($id)
	{
		$query = '
			SELECT
				m.name AS modulename,
				m.icon AS moduleicon,
				m.link_url,
				parent AS parentmodule
			FROM rtmodules AS m
			LEFT JOIN rtmodulesgroups
				ON m.moduleId = rtmodulesgroups.moduleid

			WHERE rtmodulesgroups.usergroupid = (
				SELECT userGroupId
				FROM rtusers
                WHERE userId = :user_id
			)
			ORDER BY position ASC
		';

		$statement = $this->pdo->prepare($query);

		$statement->bindValue(':user_id', $id, \PDO::PARAM_INT);

		$statement->execute();

		$result = $statement->fetchAll(\PDO::FETCH_ASSOC);
		return empty($result) ? [] : $result;
	}

	public static function getMenuForLoggedInUser()
	{
		$menu = new self();

		return $menu->getMenuForUserId(LoggedInUser::getUserId());
	}
}
