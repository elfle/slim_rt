<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 11.12.14.
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


class Playlist extends BaseModel
{
	public function getListForUser($user_id)
	{
		$sql = "
		SELECT * FROM rtplaylists AS p
		WHERE p.owner_id = :owner_id
		";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':owner_id', $user_id, \PDO::PARAM_INT);
		$statement->execute();

		return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function load($id = 0)
	{
		$sql = "
		SELECT * FROM rtplaylists AS p
		WHERE p.id = :id
		";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':id', $id, \PDO::PARAM_INT);
		$statement->execute();


		return $statement->fetch(\PDO::FETCH_ASSOC);
	}

	public function create($title, $owner_id, $items)
	{
//		echo 'create called';
		// @todo: validate!!!

		$sql = "
		INSERT INTO rtplaylists
		SET
		title = :title,
		owner_id = :owner_id,
		created_at = NOW(),
		updated_at = NOW()
		";

		try
		{
			$this->pdo->beginTransaction();

			$statement = $this->pdo->prepare($sql);

			$statement->bindValue(':title', $title, \PDO::PARAM_STR);
			$statement->bindValue(':owner_id', $owner_id, \PDO::PARAM_INT);
			$statement->execute();

			$playlist_id = $this->pdo->lastInsertId();

			// save items - no return, exception will be thrown if data is not ok
			$this->insertItems($playlist_id, $items);

			$this->pdo->commit();
			return $playlist_id;
		}
		catch(\Exception $ex)
		{
//			var_dump('save', $ex->getMessage());
			$this->pdo->rollBack();
			return 0;
		}
	}

	public function update($id, $title, $owner_id, $items)
	{
//		echo 'update called';
// @todo: validate!!!

		$sql = "
		UPDATE rtplaylists
		SET
		title = :title,
		owner_id = :owner_id,
		updated_at = NOW()
		WHERE id = :id
		";

		try
		{
			$this->pdo->beginTransaction();

			$statement = $this->pdo->prepare($sql);

			$statement->bindValue(':id', $id, \PDO::PARAM_INT);
			$statement->bindValue(':title', $title, \PDO::PARAM_STR);
			$statement->bindValue(':owner_id', $owner_id, \PDO::PARAM_INT);
			$statement->execute();

			// save items - no return, exception will be thrown if data is not ok
			$this->insertItems($id, $items);

			$this->pdo->commit();
			return $id;
		}
		catch(\Exception $ex)
		{
			var_dump($ex->getMessage());
			$this->pdo->rollBack();
			return 0;
		}
	}

	public function insertItems($playlist_id, $items)
	{
		// first, remove all previous
		$this->pdo->exec('DELETE FROM rtplaylist_items WHERE playlist_id = ' . (int)$playlist_id);

		$sql = "
		INSERT INTO rtplaylist_items
		SET
		playlist_id = :playlist_id,
		order_id = :order_id,
		resource_id = :resource_id,
		duration_ms = :duration_ms
		";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':playlist_id', $playlist_id, \PDO::PARAM_STR);


		foreach($items as $item)
		{
			$order_id = $item['order_id'];
			$resource_id = $item['resource_id'];
			$duration_ms = isset($item['duration_ms']) ? $item['duration_ms'] : 2000;

			$statement->bindValue(':order_id', 		$order_id, \PDO::PARAM_INT);
			$statement->bindValue(':resource_id', 	$resource_id, \PDO::PARAM_INT);
			$statement->bindValue(':duration_ms', 	$duration_ms, \PDO::PARAM_INT);
			$statement->execute();
		}
	}
}
