<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 11.12.14.
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


class PlaylistItem extends BaseModel
{
	public function loadByPlaylistId($playlist_id = 0)
	{
		$sql = "
		SELECT pi.*, a.location
		FROM rtplaylist_items AS pi
		INNER JOIN rtassets AS a
			ON pi.resource_id = a.assetId
		WHERE pi.playlist_id = :playlist_id
		ORDER BY order_id ASC
		";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':playlist_id', $playlist_id, \PDO::PARAM_INT);
		$statement->execute();


		return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function saveForPlaylist()
	{

	}
}
