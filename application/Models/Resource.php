<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 11.12.14.
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


class Resource extends BaseModel
{
	public function loadForUserId($userId = 0)
	{
		$sql = "
		SELECT * FROM rtassets AS a
		WHERE a.clientid = :clientid
		";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':clientid', $userId, \PDO::PARAM_INT);
		$statement->execute();


		return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function save()
	{

	}
}
