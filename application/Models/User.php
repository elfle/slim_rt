<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


class User extends BaseModel
{
	protected $table = 'rtusers';

	public function getUserByUsernameAndPassword($username, $password)
	{
		$table = $this->getTable();

		$sql = "
		SELECT * FROM $table
		WHERE username = :username
		AND password = :password";

		$statement = $this->pdo->prepare($sql);

		$statement->bindValue(':username', $username, \PDO::PARAM_STR);
		$statement->bindValue(':password', $password, \PDO::PARAM_STR);
		$statement->execute();


		return $statement->fetch(\PDO::FETCH_ASSOC);
	}
}
