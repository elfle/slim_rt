<?php
/**
 * database config array
 *
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 09.11.14.
 * Time: 16:59
 *
 */

return array(
	'host' => 'localhost',
	'dbname' => 'wsdb',
	'user' => 'root',
	'pass' => ''
);
