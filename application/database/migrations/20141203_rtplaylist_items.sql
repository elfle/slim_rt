
--
-- Table structure for table `rtplaylist_items`
--

CREATE TABLE IF NOT EXISTS `rtplaylist_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `resource_id` int(10) unsigned NOT NULL,
  `duration_ms` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `playlist_id` (`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
