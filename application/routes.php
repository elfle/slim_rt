<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 08.11.14.
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */

	/**
	 * Step 3: Define the Slim application routes
	 *
	 * Here we define several Slim application routes that respond
	 * to appropriate HTTP request methods. In this example, the second
	 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
	 * is an anonymous function.
	 */

	// Elf: let's define some route middleware (filters)

	$requireLoggedInUser = function()
	{
			if(!App\Core\LoggedInUser::isLoggedIn())
			{
				$app = \Slim\Slim::getInstance();
				$app->redirect('/login');
			}
	};

// GET route
	$app->get(
		'/login',
		function()
		{
			$controller = new App\Controllers\LoginController();
			$controller->login();
		}
	);
	$app->post(
		'/ajaxLogin',
		function()
		{
			$controller = new App\Controllers\LoginController();
			$controller->ajaxLogin();
		}
	);

	$app->map('/logout', function() use($app)
	{
		\App\Core\LoggedInUser::logOut();
		$app->redirect('/');

	})->via('GET', 'POST');

	/*
	 * if we need multiple methods, use
	 *
	$app->map('/foo/bar', function() {
		echo "I respond to multiple HTTP methods!";
 	})->via('GET', 'POST');
	 */

	$app->get(
		'/',
		$requireLoggedInUser,
		function()
		{
			$controller = new App\Controllers\DashboardController();
			$controller->index();
		}
	);

	// playlist CRUD group
	$app->group('/playlist', function () use ($app)
	{

		$controller = new App\Controllers\SlidesPlaylistController();

		// index
		$app->map('/', function() use ($controller)
		{
			$controller->index();
		})->via('GET', 'POST');

		// showList
		$app->map('/showList', function() use ($controller)
		{
			$controller->showList();
		})->via('GET', 'POST');

		// get data
		$app->post('/load', function() use ($controller)
		{
			$controller->load();
		});

		// save the list
		$app->post('/save', function() use ($controller)
		{
			$controller->save();
		});
	});


