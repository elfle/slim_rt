<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">

	<!-- Viewport Metatag -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<!-- Plugin Stylesheets first to ease overrides -->
	<link rel="stylesheet" href="plugins/plupload/jquery.plupload.queue.css" media="screen">

	<!-- Required Stylesheets -->
	<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/fonts/ptsans/stylesheet.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/fonts/icomoon/style.css" media="screen">

	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/mws-style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/icons/icol16.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/icons/icol32.css" media="screen">

	<!-- Demo Stylesheet -->
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/demo.css" media="screen">

	<!-- jQuery-UI Stylesheet -->
	<link rel="stylesheet" type="text/css" href="/vendor/jui/css/jquery.ui.all.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/jui/jquery-ui.custom.css" media="screen">

	<!-- Theme Stylesheet -->
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/mws-theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/vendor/mws/css/themer.css" media="screen">

	<!-- playlist app CSS -->
	<link rel="stylesheet" type="text/css" href="/js_app/css/playlist.css" />

	<title>RT Admin - Dashboard</title>

</head>

<body>

<!-- Header -->
<div id="mws-header" class="clearfix">

	<!-- Logo Container -->
	<div id="mws-logo-container">

		<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
		<div id="mws-logo-wrap">
			<img src="/vendor/mws/images/mws-logo.png" alt="mws admin">
		</div>
	</div>

	<!-- User Tools (notifications, logout, profile, change password) -->
	<div id="mws-user-tools" class="clearfix">

		<!-- User Information and functions section -->
		<div id="mws-user-info" class="mws-inset">

			<!-- User Photo -->
			<div id="mws-user-photo">
				<img src="/example/profile.jpg" alt="User Photo">
			</div>

			<!-- Username and Functions -->
			<div id="mws-user-functions">
				<div id="mws-username">
					Hello, <?= $logged_user['username'] ?>
				</div>
				<ul>
					<li><a href="#">Profile</a></li>
					<li><a href="#">Change Password</a></li>
					<li><a href="/logout">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Start Main Wrapper -->
<div id="mws-wrapper">

<!-- Necessary markup, do not remove -->
<div id="mws-sidebar-stitch"></div>
<div id="mws-sidebar-bg"></div>

<!-- Sidebar Wrapper -->
<div id="mws-sidebar">

	<!-- Hidden Nav Collapse Button -->
	<div id="mws-nav-collapse">
		<span></span>
		<span></span>
		<span></span>
	</div>

	<!-- Main Navigation -->
	<div id="mws-navigation">
		<ul>
			<?php foreach($menu_items as $item): ?>
				<li>
					<a href="#" data-link="<?= isset($item['link_url']) ? $item['link_url'] : '' ?>">
						<i class="<?= $item['moduleicon']?>"></i><?= $item['modulename'] ?>
					</a>
				</li>
			<?php endforeach; ?>
			<li data-role="edit_playlist"></li>
		</ul>
	</div>
</div>

<!-- Main Container Start -->
<div id="mws-container" class="clearfix">

<!-- Inner Container Start -->
<div class="container" data-role="container">


<div class="mws-panel grid_8">
<div class="mws-panel-header">
	<span><i class="icon-table"></i>Popis uređaja</span>
</div>
<div class="mws-panel-body no-padding">
<table class="mws-datatable-fn mws-table">
<thead>
<tr>
	<th>Oznaka</th>
	<th>Naziv</th>
	<th>Status</th>
	<th></th>
</tr>
</thead>
<tbody>
<tr>
	<td>Trident</td>
	<td>Internet
		Explorer 4.0
	</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Trident</td>
	<td>Internet
		Explorer 5.0
	</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Trident</td>
	<td>Internet
		Explorer 5.5
	</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Trident</td>
	<td>Internet
		Explorer 6
	</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Trident</td>
	<td>Internet Explorer 7</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Trident</td>
	<td>AOL browser (AOL desktop)</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Firefox 1.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Firefox 1.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Firefox 2.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Firefox 3.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Camino 1.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Camino 1.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Netscape 7.2</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Netscape Browser 8</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Netscape Navigator 9</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.1</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.2</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.3</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.4</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.6</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.7</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Mozilla 1.8</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Seamonkey 1.1</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Gecko</td>
	<td>Epiphany 2.20</td>
	<td>Nepoznat</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>Safari 1.2</td>
	<td>Nedostupan</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>Safari 1.3</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>Safari 2.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>Safari 3.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>OmniWeb 5.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>iPod Touch / iPhone</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Webkit</td>
	<td>S60</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 7.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 7.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 8.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 8.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 9.0</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 9.2</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera 9.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Opera for Wii</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Nokia N800</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Presto</td>
	<td>Nintendo DS browser</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>KHTML</td>
	<td>Konqureror 3.1</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>KHTML</td>
	<td>Konqureror 3.3</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>KHTML</td>
	<td>Konqureror 3.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Tasman</td>
	<td>Internet Explorer 4.5</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Tasman</td>
	<td>Internet Explorer 5.1</td>
	<td>OK</td>
	<td class=" ">
		<span class="btn-group">
			<a class="btn btn-small" href="#"><i class="icon-search"></i></a>
			<a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
			<a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
		</span>
	</td>
</tr>
<tr>
	<td>Tasman</td>
	<td>Internet Explorer 5.2</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>NetFront 3.1</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>NetFront 3.4</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>Dillo 0.8</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>Links</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>Lynx</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>IE Mobile</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Misc</td>
	<td>PSP browser</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
<tr>
	<td>Other browsers</td>
	<td>All others</td>
	<td>OK</td>
	<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="#"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small" href="#"><i class="icon-trash"></i></a>
                                        </span>
	</td>
</tr>
</tbody>
</table>
</div>
</div>


</div>

<!-- Inner Container End -->

<!-- Footer -->
<div id="mws-footer" style="bottom: -1px;">
	Reklamna Tocka 2014. All Rights Reserved.
</div>

</div>
<!-- Main Container End -->

</div>

<!-- JavaScript Plugins -->
<script src="/vendor/mws/js/libs/jquery-1.8.3.min.js"></script>
<script src="/vendor/mws/js/libs/jquery.mousewheel.min.js"></script>
<script src="/vendor/mws/js/libs/jquery.placeholder.min.js"></script>
<script src="/plugins/fileinput.js"></script>

<!-- jQuery-UI Dependent Scripts -->
<!--<script src="/vendor/jui/js/jquery-ui-1.9.2.min.js"></script>-->
<script src="/vendor/jquery-ui-1.11.2/jquery-ui.min.js"></script>
<script src="/vendor/jui/jquery-ui.custom.min.js"></script>
<script src="/vendor/jui/js/jquery.ui.touch-punch.js"></script>

<!-- Plugin Scripts -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<!--[if lt IE 9]>
<script src="/vendor/mws/js/libs/excanvas.min.js"></script>
<![endif]-->
<!--
<script src="/plugins/flot/jquery.flot.min.js"></script>
<script src="/plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
<script src="/plugins/flot/plugins/jquery.flot.pie.min.js"></script>
<script src="/plugins/flot/plugins/jquery.flot.stack.min.js"></script>
<script src="/plugins/flot/plugins/jquery.flot.resize.min.js"></script>
-->
<script src="/plugins/colorpicker/colorpicker-min.js"></script>
<script src="/plugins/validate/jquery.validate-min.js"></script>
<script src="custom-plugins/wizard/wizard.min.js"></script>

<!-- Core Script -->
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendor/mws/js/core/mws.js"></script>
<script src="/vendor/mws/js/rt/rt2.js"></script>

<script src="/plugins/plupload/plupload.full.min.js"></script>
<script src="/plugins/plupload/moxie.js"></script>
<script src="/plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<script src="/plugins/plupload/jquery.ui.plupload/jquery.ui.plupload.js"></script>


<!-- load require.js - needed for slide playlist edit -->
<!-- MUST be loaded last so other scripts do not see that amd is available -->
<script data-main="js_app/main" src="js_app/vendor/requirejs/require.js"></script>
</body>
</html>
