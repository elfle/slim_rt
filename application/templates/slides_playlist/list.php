<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i>Popisi</span>
	</div>
	<div class="mws-panel-body no-padding">
		<table class="mws-datatable-fn mws-table">
			<thead>
			<tr>
				<th>Naziv</th>
				<th>Datum zadnje izmjene</th>
				<th>Opcije</th>
			</tr>
			</thead>
			<tbody data-role="list">
				<?php foreach($list as $row) : ?>
					<tr>
						<td><?= $row['title'] ?></td>
						<td><?= $row['created_at'] ?></td>
						<td>
							<button type="button" data-role="edit" data-id="<?= $row['id'] ?>" class="btn btn-info">Promijeni</button>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div class="mws-panel-body">
		<button type="button" data-role="create" class="btn btn-info pull-right">Kreiraj</button>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">
	(function()
	{
		$('[data-role="create"]').on('click', function()
		{
			// defined in /public/js_app/main.js
			module_loaders.playlist_edit();
		})

		$('[data-role="list"]').on('click', '[data-role="edit"]', function(event)
		{
			var $btn = $(event.currentTarget);
			var id = $btn.data('id');

			module_loaders.playlist_edit(id);
		})

	})();

</script>
