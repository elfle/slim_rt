<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Elf
 * Date: 08.11.14.
 * Time: 18:22
 * To change this template use File | Settings | File Templates.
 */

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
//require '../vendor/slim/slim/Slim/Slim.php';
	// nope, require composers autoload
require '../vendor/autoload.php';

\Slim\Slim::registerAutoloader();

	/**
	 * in-between step: define paths
	 */


	define('PATH_APP', __DIR__ . '/../application');
	define('PATH_PUBLIC', __DIR__ . '/../public');

	/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(
	[
		'debug' => true,
    	'templates.path' => PATH_APP . '/templates'
	]
);

// call routes
require(PATH_APP . '/routes.php');
