<?php

	// new \PDO("mysql:host=localhost;dbname=wsdb", 'root', '');

	// custom: invoke bootstrap to set up paths and start app
	include('../bootstrap/start.php');



/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
