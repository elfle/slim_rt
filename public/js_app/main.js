/**
 * Created with JetBrains PhpStorm.
 * User: Elf
 * Date: 18.10.14.
 * Time: 20:04
 * To change this template use File | Settings | File Templates.
 */
require.config({
	baseUrl: 'js_app',
	urlArgs: "bust=" +  (new Date()).getTime(), // cache busting
	paths: {
		// main libs
		jquery: 'vendor/jquery/loader',
		backbone: 'vendor/backbone/backbone',
		marionette: 'vendor/marionette/backbone.marionette.min',
		underscore: 'vendor/underscore/underscore',
		handlebars: 'vendor/handlebars/handlebars',
		text: 'vendor/requirejs-text/text'
	}
});


// expose functionality to rest of system
window.module_loaders = {
	playlist_edit: function(playlist_id)
	{
		require(
			['require', 'jquery', 'backbone', 'modules/slide_playlist/boot'],
			function(require, $, Backbone, playlist_edit)
			{
				playlist_edit.init(playlist_id);
			}
		);

	}
};
