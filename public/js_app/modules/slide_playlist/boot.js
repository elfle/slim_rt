define(
	[
		'modules/slide_playlist/event_proxy',
		'modules/slide_playlist/layout',
		'modules/slide_playlist/resource/resource.collection',
		'modules/slide_playlist/resource/resource.list.view',
		'modules/slide_playlist/playlist/collection',
		'modules/slide_playlist/playlist/collection.view',
		'modules/slide_playlist/playlist/data.model',
		'modules/slide_playlist/playlist/data.view',
		'modules/slide_playlist/preview/player.view'
	],
	function(
		eventProxy,
		MainLayout,
		ResourceCollection,
		ResourceListView,
		PlaylistCollection,
		PlaylistCollectionView,
		PlaylistData,
		PlaylistDataView,
		PlayerView
	)
	{
		function init(playlist_id)
		{
			playlist_id && playlist_id > 0 || (playlist_id = 0);
			var layout = new MainLayout({});

			var $container = $('[data-role="container"]');

			layout.render();
			$container.html(layout.$el);

			var resource_collection = new ResourceCollection();
			var resource_list_view = new ResourceListView(
				{
					collection: resource_collection,
					eventProxy: eventProxy
				});

			// proxy dragstart & dragstop so we activate/deactivate droppable fields in the playlist
			resource_list_view
				.on('childview:dragstart', function(view, event, ui)
				{
					eventProxy.trigger('resourcedragstart');
				})
				.on('childview:dragstop', function(view, event, ui)
				{
					eventProxy.trigger('resourcedragstop');
				});

			var playlist_data_model = new PlaylistData();

			var playlist_data_view = new PlaylistDataView({
				el: '[data-role="options_element"]',
				model: playlist_data_model
			});

			playlist_data_view.render();

			var playlist_collection = new PlaylistCollection();
			var playlist_collection_view = new PlaylistCollectionView(
				{
					collection: playlist_collection,
					eventProxy: eventProxy
				});

			layout.resource_list.show(resource_list_view);
			layout.playlist.show(playlist_collection_view);

			var player = new PlayerView({
				el: $('[data-region="preview"]'),
				collection: playlist_collection
			});

			$('[data-role="toggle_player_playing"]').on('click', _.bind(player.togglePlay, player));


			// LOAD

			var url = '/playlist/load';
				// get data
			$.ajax({
				type:'POST',
				url: url,
				dataType: 'json',
				data: {id: playlist_id},
				success: function(response)
				{
					if(response && response.resources)
					{
						resource_collection.reset(response.resources);
						playlist_collection.reset(response.items);
						playlist_data_model.clear();
						playlist_data_model.set(response.playlist);
					}
				}
			});

//			resource_collection.url = 'server_test/resources.json';
//			resource_collection.fetch();
//			playlist_collection.url = 'server_test/playlist.json';
//			playlist_collection.fetch();

			// SAVE
			var save_url = '/playlist/save';
			$container.off('.playlist').on('click.playlist', '.btn[data-role="save"]', function()
			{
				$.ajax({
					type:'POST',
					url: save_url,
					dataType: 'json',
					data: {
						playlist: playlist_data_model.toJSON(),
						items: playlist_collection.toJSON()
					}

				}).done(function(){console.log(arguments)});
			});
		};

		return {
			init: init
		};
	}
);
