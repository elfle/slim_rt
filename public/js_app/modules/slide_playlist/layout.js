define(
	['marionette', 'handlebars', 'text!modules/slide_playlist/templates/layout.html'],
	function(Marionette, Handlebars, layoutTemplate)
	{
		var template_function = Handlebars.compile(layoutTemplate);

		return Marionette.LayoutView.extend(
			{
				template: template_function,
				className: 'playlist-layout-container-all greyish',
				regions: {
					resource_list: '[data-region="resource_list"]',
					preview: '[data-region="preview"]',
					playlist: '[data-region="items_list"]'
				}
			});

	}
);
