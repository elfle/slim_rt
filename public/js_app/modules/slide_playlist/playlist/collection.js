define(
	['underscore', 'backbone', 'modules/slide_playlist/playlist/model'],
	function(_, Backbone, PlaylistModel)
	{
		var PlaylistCollection = Backbone.Collection.extend(
			{
				model: PlaylistModel,
				comparator: 'order_by',

				_current_max_order_id: 0,

				initialize: function()
				{
					this.on('add', this._setupModelOrderId);
					window.ccc = this;
				},

				_setupModelOrderId: function(model, collection, options)
				{
					this._reorderModels();
				},

				_reorderModels: function()
				{
					var order_id = 1;
					this.each(function(m)
					{
						m.attributes.order_id = order_id++;
					})
				},

				parse: function(resp, options)
				{
					this.playlistOptions = resp;
					return resp.items;
				},

				addResourceModel: function(resourceData)
				{
					console.log('adding', resourceData)
					this.add(resourceData);
				}
			});

		return PlaylistCollection;
	}
);
