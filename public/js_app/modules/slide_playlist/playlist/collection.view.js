define(
	['marionette', 'handlebars', 'modules/slide_playlist/playlist/model.view', 'text!modules/slide_playlist/templates/playlist_list.html'],
	function (Marionette, Handlebars, PlaylistModelView, viewTemplate)
	{
		var template_function = Handlebars.compile(viewTemplate);

		var PlaylistCollectionView = Marionette.CompositeView.extend(
			{
				template: template_function,
				className: 'playlist-items-container',
				childView: PlaylistModelView,
				childViewContainer: '@ui.track',

				ui: {
					head: '[data-role="playlist_head"]',
					track: '[data-role="playlist_track"]',
					prev: '[data-role="playlist_prev"]',
					next: '[data-role="playlist_next"]'
				},

				playlist: {
					current_index: 1
				},

				events: {
					// click to slide
					'click @ui.prev': 'trackSlideLeft',
					'click @ui.next': 'trackSlideRight',
					// when dragging and hovering over arrow, start sliding
					'mouseenter @ui.prev.draggable_resource_active': 'trackSlideLeftStart',
					'mouseleave @ui.prev.draggable_resource_active': 'trackSlideStop',
					'mouseenter @ui.next.draggable_resource_active': 'trackSlideRightStart',
					'mouseleave @ui.next.draggable_resource_active': 'trackSlideStop',

					// triggered by sortable, tell child views to update their models with new ordering
					'sortupdate': 'updateOrder'
				},

				initialize: function(options)
				{
					this.eventProxy = options.eventProxy;

					this.eventProxy.on('resourcedragstart', this.activateDroppable, this);
					this.eventProxy.on('resourcedragstop', this.deactivateDroppable, this);
				},

				getItemsToShowCount: function()
				{
					return 5;
				},

				getDesiredItemWidth: function()
				{
					var availableHeadWidth = this.ui.head.width();

					return Math.floor(availableHeadWidth / this.getItemsToShowCount());
				},

				/**
				 * tell child views to activate draggable functionality
				 */
				activateDroppable: function()
				{
					if(this.children.length)
					{
						this.children.invoke('activateDraggable');
						this.activateHoverablePrevNext();
					}
					else
					{
						this.ui.head.droppable({
							activeClass: "show",
							hoverClass: "hover",
							tolerance: 'pointer',
							drop: _.bind(this.onDroppableDrop, this)
						});
					}
				},

				onDroppableDrop: function(event, ui)
				{
					this.collection.add(ui.helper[0].modelData, {at: 1});
					this.ui.head.droppable('destroy');
				},

				/**
				 * tell child views to deactivate draggable functionality
				 */
				deactivateDroppable: function()
				{
					this.children.invoke('deactivateDraggable');
					this.deactivateHoverablePrevNext();
					this.trackSlideStop();
				},

				/**
				 * initialize sortable
				 */
				onRender: function()
				{
					this.ui.track
						.sortable({
							items: ".playlist-items-track-item:not(.placeholder)",
//							axis: "x",
							tolerance: 'pointer',
							refreshPositions: true
							/*,
							// this messes up sortable and playlist track items get pushed to next row - they disappear
							start: _.bind(this.activateHoverablePrevNext, this),
							stop: _.bind(this.deactivateHoverablePrevNext, this)
							*/
						}
					);
				},

				activateHoverablePrevNext: function()
				{
					this.ui.prev.addClass('draggable_resource_active');
					this.ui.next.addClass('draggable_resource_active');
				},
				deactivateHoverablePrevNext: function()
				{
					this.ui.prev.removeClass('draggable_resource_active');
					this.ui.next.removeClass('draggable_resource_active');
				},

				onAddChild: function(view)
				{
					this.setItemDimensions(view.$el);
					this.trackRecalculateDimensions();
				},

				onRemoveChild: function()
				{
					this.trackRecalculateDimensions();
					this.trackVerifyHeadPosition();
				},

				setItemDimensions: function($el)
				{
					$el.width(this.getDesiredItemWidth());
					$el.height(this.ui.head.height() - 5);
				},

				updateOrder: function()
				{
					var index = 1;
					// get all playlist items and trigger order updating
					this.ui.track.children().each(function(ind, el)
					{
						$(el).trigger('updateOrderId', {order_id: index++});
					});

					this.collection.sort();
				},

				trackSlideLeft: function()
				{
					this.trackSlideTo(this.playlist.current_index - 1);
				},

				trackSlideRight: function()
				{
					this.trackSlideTo(this.playlist.current_index + 1);
				},

				trackSlideLeftStart: function()
				{
					this.trackSlideStop();
					this.trackSlideLeft();
					this.slide_interval_id = window.setInterval(_.bind(this.trackSlideLeft, this), 500);
				},

				trackSlideRightStart: function()
				{
					this.trackSlideStop();
					this.trackSlideRight();
					this.slide_interval_id = window.setInterval(_.bind(this.trackSlideRight, this), 500);
				},
				trackSlideStop: function()
				{
					this.slide_interval_id && window.clearInterval(this.slide_interval_id);
					this.slide_interval_id = 0;
				},

				trackSlideTo: function(targetSlide)
				{
					var max_target_slide = this.children.length - this.getItemsToShowCount() + 1;

					if(targetSlide == this.playlist.current_index || targetSlide < 1 || targetSlide > max_target_slide)
					{
						this.trackSlideStop();
						return;
					}

					// calculate
					var previousItemsWidth = 0;
					this.ui.track.children().slice(0, targetSlide - 1).each(function()
					{
						previousItemsWidth += $(this).outerWidth(true);
					});

					var $container = this.$el;
					var that = this;
					this.ui.track.animate(
						{left: -previousItemsWidth},
						'fast',
						// this here is instead of telling draggable to always refresh positions
						// this gets delegated to resource list view which tells containing draggables to update
						function(){that.eventProxy.trigger('update_draggable_positioning')}
					);
					this.playlist.current_index = targetSlide;
				},
				trackRecalculateDimensions: function()
				{
					this._trackSetTrackWidth();
				},

				/**
				 * verify that head is displaying all available items - no gaps at the end
				 */
				trackVerifyHeadPosition: function()
				{
					var to_show = this.getItemsToShowCount();
					if(this.playlist.current_index + to_show >= this.children.length)
					{
						var new_target = this.children.length - this.getItemsToShowCount() + 1;
						this.trackSlideTo(new_target > 0 ? new_target : 1);
					}
				},
				_trackSetTrackWidth: function()
				{
					var width = 0;
					this.children.each(function(child)
					{
						width += child.$el.outerWidth(true);
					});

					this.ui.track.width(width + 100);
				}
			}
		);

		return PlaylistCollectionView;
	}
);
