define(
	['underscore', 'backbone'],
	function(_, Backbone)
	{
		var PlaylistDataModel = Backbone.Model.extend(
			{
				defaults: {
					id: 0,
					title: 'bez imena',
					item_duration: 2
				},
				validate: function()
				{
					var errors = {};

					if(!this.get('title'))
					{
						errors['title'] = 'Molimo, unesite naziv';
					}

					if(!this.get('item_duration') || this.get('item_duration') < 1)
					{
						errors['item_duration'] = 'Molimo, unesite trajanje';
					}

					this.trigger('validated', errors);
				}
			}
		);

		return PlaylistDataModel;
	}
);


