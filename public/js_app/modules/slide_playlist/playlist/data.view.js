define(
	['marionette', 'handlebars', 'text!modules/slide_playlist/templates/playlist_data.html'],
	function(Marionette, Handlebars, itemTemplate)
	{
		var PlaylistDataView = Marionette.ItemView.extend(
			{
				template: Handlebars.compile(itemTemplate),

				modelEvents: {
					'change': 'render',
					'validated': 'onModelValidated'
				},

				events: {
					'change [data-model]': 'updateModelField'
				},

				updateModelField: function(event)
				{
					var $el = $(event.currentTarget);

					this.model.set($el.data('model'), $el.val(), {silent: true});
					this.model.validate();
				},

				onModelValidated: function(errors)
				{
					// remove errors
					this.$el.find('.mws-form-row.error').removeClass('error').find('.mws-error').empty().hide();

					if(!_.isEmpty(errors))
					{
						var $container = this.$el;
						_.each(errors, function(msg, field)
						{
							$container
								.find('[data-model="' + field + '"]')
								.parent()
								.addClass('error')
								.find('.mws-error')
								.text(msg)
								.show();
						});
					}
				}
			});

		return PlaylistDataView;
	}
);
