define(
	['underscore', 'backbone'],
	function(_, Backbone)
	{
		return Backbone.Model.extend(
			{
				defaults: {
					resource_id: 0,
					name: '',
					resource_url: ''
				},
				idAttribute: 'order_id',
				initialize: function()
				{
					// get resource_id
					$asset_id = this.get('assetId');
					if($asset_id)
					{
						this.set('resource_id', $asset_id);
					}
				},
				insertModelBeforeSelf: function(data)
				{
					this.collection.add(data, {at: this.collection.indexOf(this)});
				},
				insertModelAfterSelf: function(data)
				{
					this.collection.add(data, {at: this.collection.indexOf(this) + 1});
				}
			}
		);
	}
);


