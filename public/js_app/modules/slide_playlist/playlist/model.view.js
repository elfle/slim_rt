define(
	['marionette', 'handlebars', 'text!modules/slide_playlist/templates/playlist_item.html'],
	function(Marionette, Handlebars, itemTemplate)
	{
		var PlaylistModelView = Marionette.ItemView.extend(
			{
				tagName: 'div',
				className: 'playlist-items-track-item',
				template: Handlebars.compile(itemTemplate),

				ui: {
					dropBefore: '.drop-zone.drop-before',
					dropAfter: '.drop-zone.drop-after',
					dropOverlay: '.droppable-overlay'
				},

				events: {
					'updateOrderId': 'updateOrderId',
					'click [data-role="remove"]': 'confirmRemove'
				},

				updateOrderId: function(event, data)
				{
					data && data.order_id && (this.model.set('order_id', data.order_id));
				},

				onRender: function()
				{
					this.ui.dropBefore.droppable({
						activeClass: "show",
						hoverClass: "hover",
						tolerance: 'pointer',
						drop: _.bind(this.onInsertBefore, this)
					});

					this.ui.dropAfter.droppable({
						activeClass: "show",
						hoverClass: "hover",
						tolerance: 'pointer',
						drop: _.bind(this.onInsertAfter, this)
					});
				},

				confirmRemove: function()
				{
					if(window.confirm('Sigurno želite ukoniti sliku?'))
					{
//						this.model.destroy();
						this.model.collection.remove(this.model);
					}
				},

				onInsertBefore: function(event, ui)
				{
					this.model.insertModelBeforeSelf(ui.helper[0].modelData);
				},
				onInsertAfter: function(event, ui)
				{
					this.model.insertModelAfterSelf(ui.helper[0].modelData);
				},

				activateDraggable: function()
				{
					this.ui.dropOverlay.removeClass('hidden');
				},
				deactivateDraggable: function()
				{
					this.ui.dropOverlay.addClass('hidden');
				}
			});

		return PlaylistModelView;
	}
);
