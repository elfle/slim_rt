define(
	['backbone', 'handlebars', 'text!modules/slide_playlist/templates/preview_screen.html'],
	function (Backbone, Handlebars, viewTemplate)
	{
		var template_function = Handlebars.compile(viewTemplate);

		var PlayerView = Backbone.View.extend(
			{
				template: template_function,
				_current_item_index: 0,
				_is_playing: false,
				initialize: function(options)
				{

				},

				play: function()
				{
					if(this._is_playing)
					{
						return;
					}

					this._is_playing = true;
					this._current_item_index = 0;
					this._playForward();
				},

				stop: function()
				{
					this._is_playing = false;
				},
				togglePlay: function()
				{
					this._is_playing ? this.stop() : this.play();
				},

				_playForward: function()
				{
					if(!this._is_playing)
					{
						return;
					}
					// show current item
					var model = this.collection.at(this._current_item_index);
					// stop if no model available
					if(!model)
					{
						this.stop();
						return;
					}

					console.log('rendering image', this.$el);
					this.render(model);


					// set next call
					this._current_item_index++;
					// @todo: read delay from collection/model
					_.delay(_.bind(this._playForward, this), 1000);
				},

				render: function(model)
				{
					this.$el.html(this.template(model.toJSON()));
				}
			}
		);

		return PlayerView;
	}
);
