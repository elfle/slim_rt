define(
	['underscore', 'backbone', 'modules/slide_playlist/resource/resource.model'],
	function(_, Backbone, ResourceModel)
	{
		return ResourceCollection = Backbone.Collection.extend(
			{
				model: ResourceModel
			});
	}
);
