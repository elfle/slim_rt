define(
	['marionette', 'handlebars', 'modules/slide_playlist/resource/resource.view', 'text!modules/slide_playlist/templates/resource_list.html'],
	function (Marionette, Handlebars, ResourceItemView, listTemplate)
	{
		var template_function = Handlebars.compile(listTemplate);

		return Marionette.CompositeView.extend(
			{
				template: template_function,
				className: 'playlist-resources-container',
				childView: ResourceItemView,
				childViewContainer: 'ul',

				initialize: function(options)
				{
					this.eventProxy = options.eventProxy;

					this.eventProxy.on('update_draggable_positioning', this.updateDraggablePositioning, this)
				},

				updateDraggablePositioning: function()
				{
					this.children.each(function(view)
					{
						$.ui.ddmanager.prepareOffsets(view.$el.draggable('instance'), 'scroll');
					})
				}
			}
		);
	}
);
