define(
	['underscore', 'backbone'],
	function(_, Backbone)
	{
		return Backbone.Model.extend(
			{
				defaults: {
					id: 0,
					name: '',
					resource_url: ''
				}
			}
		);
	}
);


