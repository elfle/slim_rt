define(
	['marionette', 'handlebars', 'text!modules/slide_playlist/templates/resource_item.html'],
	function(Marionette, Handlebars, itemTemplate)
	{
		return ResourceItemView = Marionette.ItemView.extend(
			{
				tagName: 'li',
				template: Handlebars.compile(itemTemplate),
				onRender: function()
				{
					var model = this.model;
					var view = this;

					this.$el.draggable({
						appendTo: "body",
						helper: function()
						{
							var $helper = $('<div class="draggable-resource-dragging"></div>').
								html(model.get('name'));

							var helper = $helper[0];
							helper.modelData = model.toJSON();
							return helper;
						},
						start: function(event, ui)
						{
							view.trigger('dragstart', event, ui);
						},
						stop: function(event, ui)
						{
							view.trigger('dragstop', event, ui);
						}
						// this has to be enabled.
						// when dragging to playlist and moving the playlist around (by hovering above prev/next)
						// droppable targets positions are not calculated properly
//						refreshPositions: true
					});
				}
			});
	}
);
