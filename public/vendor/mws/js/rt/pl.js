/**
 * Created by khumche on 16.10.2014..
 */
// Initialize the widget when the DOM is ready
$(function() {
    console.log("lovro was here");
$("#uploader").pluploadQueue({
    // General settings
    runtimes: 'html5, html4',
    url: 'php/upload.php',
    max_file_size: '1000mb',
    max_file_count: 20, // user can add no more then 20 files at a time
    chunk_size: '1mb',
    unique_names: true,
    multiple_queues: true,

    // Resize images on clientside if we can
    resize: {
        width: 320,
        height: 240,
        quality: 90
    },

    // Rename files by clicking on their titles
    rename: true,

    // Sort files
    sortable: true,
    thumbs: true,

    // Specify what files to browse for
    filters: [{
        title: "Image files",
        extensions: "jpg,gif,png,mp4"
    }, {
        title: "Zip files",
        extensions: "zip,avi"
    }]
});
});