(function($, window)
{

	var navigation = window.navigation = {
		init: function()
		{
			var that = this;

			$('#mws-navigation li a').click(function() {
				var $el = $(this);

				that.highlightMenuItem($el);

				var url = $el.data('link');

				that.loadContent(url);
			});
		},

		highlightMenuItem: function($selectedElement)
		{
			$selectedElement.parent().siblings('li').removeClass('active');
			$selectedElement.parent().addClass('active');
		},

		loadContent: function(url)
		{
			var $contentDiv = $('.container');

			$.ajax({
				type:'POST',
				url: url,
				dataType: 'json',
				success: function(response)
				{
					if(response.success && response.content)
					{
						$contentDiv.html(response.content);
					}
				}
			})
		}
	};

    $(document).ready(function()
    {
		navigation.init();

        // Data Tables
        //console.log($('.mws-datatable-fn.mws-table'), $.fn.dataTable);
        $('.mws-datatable-fn.mws-table').dataTable({
            "aoColumns": [
                null,
                null,
                null,
                { "bSortable": false }
            ]
        });
    });


})(jQuery, window);
